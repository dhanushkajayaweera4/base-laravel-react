import React, { Fragment, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory, Redirect } from 'react-router-dom';
import { RootState } from '../reducers'
import { ToastContainer, toast } from 'react-toastify';

const NotificationHandler = () => {
    const notification = useSelector((state: RootState) => state.notifications.notification)
    const [errorCode, setErrorCode] = useState<number>(200)
    const history = useHistory()
    React.useEffect(() => {
        if (notification) {
            if (notification.type === "error") {
                if (notification.error.status === 404) {
                    setErrorCode(404)
                    return
                }
                toast.error(notification.message)
            }
            else if (notification.type === "success") {
                toast.success(notification.message)
            }
        }
    }, [notification])
    return (
        <Fragment>
            <ToastContainer />
            {errorCode === 404 ? <Redirect to={{ pathname: '/404' }} /> : ''}
        </Fragment>
    )
}

export default NotificationHandler
