import { IAuthState } from './../reducers/auth';
import axios from 'axios';
import { RootState } from '../reducers';

export const loadState = (): RootState => {
    try {
        const serializedData = localStorage.getItem('session')
        if (serializedData === null) {
            return undefined
        }
        const authState: IAuthState = JSON.parse(atob(serializedData))
        axios.defaults.headers.common['Authorization'] = authState.auth.token_type + ' ' + authState.auth.access_token;
        return { auth: authState, users: void 0, catalogs: void 0, notifications: void 0 }
    } catch (error) {
        return undefined
    }
}

export const saveState = (state: RootState) => {
    try {
        let serializedData = JSON.stringify(state.auth)
        localStorage.setItem('session', btoa(serializedData))
    } catch (error) {
        console.log(error)
    }
}
