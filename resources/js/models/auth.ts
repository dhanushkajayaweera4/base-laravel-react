import { IUser, User } from './user';

export type TAuthForm = {
    email: string;
    password?: string;
    password_confirmation?: string;
    token?: string;
}
export interface IAuth {
    user?: IUser;
    access_token: string;
    token_type: string;
    refresh_token?: string;
    expires_in: number;
    expires_at?: number;
};

export class Auth implements IAuth {
    user: User;
    access_token: string;
    token_type: string;
    refresh_token: string;
    expires_in: number;
    expires_at: number;

    constructor(data: { [k: string]: any }) {
        this.access_token = data.access_token;
        this.token_type = data.token_type;
        this.refresh_token = data.refresh_token ?? null;
        this.expires_in = data.expires_in;
        this.expires_at = Date.now() + this.expires_in;
        this.user = data.user ? User.fromJson(data.user) : null;
    }
}
