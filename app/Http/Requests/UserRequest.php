<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;
use App\Exceptions\UnprocessableEntityHttpException;
use App\Traits\FileAction;

class UserRequest extends FormRequest
{
    use FileAction;

    /**
     * The user files data.
     *
     * @var array
     */
    public $fileData = [];

    /**
     * The user file types.
     *
     * @var array
     */
    public $fileTypes = ['avatar'];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!$this->routeIs('api.v1.users.updateMe') && !$this->routeIs('api.v1.users.register')) {
            return $this->user()->role->slug === 'admin';
        }
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation() {
        $this->merge([
            'email' => strtolower($this->input('email')),
            'email_confirmation' => strtolower($this->input('email_confirmation')),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'role_id' => 'required|integer|exists:roles,id',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'nullable|regex:/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$/',
            'email' => 'required|email|unique:users,email',
            'password' => 'nullable|min:8|confirmed',
        ];
        // Validate files data
        foreach ($this->fileTypes as $type) {
            // Validate file as array
            $rules["{$type}_file"] = 'nullable|array';
            $rules["{$type}_file".".name"] = "required_with:"."{$type}_file";
            $rules["{$type}_file".".contents"] = "required_with:"."{$type}_file";
        }
        // If is and update request
        if (strtoupper($this->method()) === 'PUT') {
            $rules['email'] = ['required', 'email', Rule::unique('users')->ignore($this->route('id'))];
            // Fields are optional on update request
            if ($this->get('password') === '********') {
                $this['password'] = null;
                $this['password_confirmation'] = null;
            }
            $rules['password'] = 'nullable|min:8|confirmed';
        }
        if ($this->routeIs('api.v1.users.updateMe')) {
            $rules['role_id'] = 'nullable';
            $rules['email'] = ['nullable', 'email', Rule::unique('users')->ignore($this->user()->id)];
        }
        else if ($this->routeIs('api.v1.users.register')) {
            $rules['role_id'] = 'nullable';
            $rules['email'] = 'required|email|confirmed|unique:users,email';
        }
        return $rules;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        // Delete uploaded files
        $file = null;
        foreach ($this->fileData as $file) {
            if ($file['path'] !== false) {
                unlink($file['storage_path']);
            }
        }
        throw new UnprocessableEntityHttpException(Lang::get("The given data was invalid."), $validator->errors(), 'invalid_data');
    }
    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $file = null;
        $this->fileData = [];
        // Base64 decode files and store them
        foreach ($this->fileTypes as $type) {
            $file = $this->input("{$type}_file");
            if (
                !empty($file) && array_key_exists('name', $file) && !empty($file['name'])
                && array_key_exists('contents', $file) && !empty($file['contents'])
            ) {
                // Decode and store file
                $this->fileData["{$type}_file"] = [
                    'name' => $file['name'],
                    'path' => $this->uploadFile($file, 'users'),
                    'storage_path' => ''
                ];
                // If file was stored, attach the storage path to array file
                if ($this->fileData["{$type}_file"]['path'] !== false) {
                    $this->fileData["{$type}_file"]['storage_path'] = storage_path($this->getStorageAppDir() . $this->fileData["{$type}_file"]['path']);
                }
                // Merge input with the new file data
                $this->merge(["{$type}_file" => $this->fileData["{$type}_file"]]);
            }
        }
        // Validate files uploaded from api
        $validator->after(function ($validator) {
            $file = null;
            foreach ($this->fileData as $i => $file) {
                // If store failed
                if ($file['path'] === false) {
                    $description = trans('validation.image', ['attribute' => trans("validation.attributes.$i")]);
                    $validator->errors()->add("$i.contents", $description);
                }
                // Validate mimetype
                else {
                    $mime = mime_content_type($file['storage_path']);
                    // Validate that file is an image
                    if (!in_array($mime, ['image/png', 'image/jpeg', 'image/gif'])) {
                        $description = trans('validation.image', ['attribute' => trans("validation.attributes.$i")]);
                        $validator->errors()->add("$i.contents", $description);
                    }
                }
            }
        });
    }
}
