## Server requirements

* apache2 or nginx
* php >= 7.2.5
* libapache2-mod-php7.2
* bcmath PHP Extension
* ctype PHP Extension
* fileinfo PHP Extension
* json PHP Extension
* mbstring PHP Extension
* openssl PHP Extension
* pdo PHP Extension
* tokenizer PHP Extension
* xml PHP Extension
* nodejs >= 12.14.0
* npm
* composer >= 2.0

## Install code

**Clone project**
```
$ git clone git@gitlab.com:segi.manzanares/base-laravel-react.git
```
**Install dependencies**
```
$ cd base-laravel-react
$ composer install
```
**Create storage link**
```
$ php artisan storage:link
```
**Install npm dependencies (on local setup)**
```
$ npm install
```

## Configure environment

**Update database settings in .env file**
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=dbname
DB_USERNAME=youruser
DB_PASSWORD=yourpass
```

**Set mail settings in the .env file**
```
MAIL_MAILER=sendmail
MAIL_FROM_ADDRESS=no-reply@moneo.domain
MAIL_FROM_NAME="${APP_NAME}"
```

**Set app settings in the .env file**
```
MIX_REACT_APP_API_URL=https://miproyecto/api/v1
MIX_REACT_APP_API_CLIENT_ID=the-client-id
MIX_REACT_APP_API_CLIENT_SECRET=the-client-secret
```

## Initialize db

**Run migrations**
```
$ php artisan migrate
```
**Seed db with default data**
```
$ php artisan db:seed
```

## Init passport

**Create keys**
```
$ php artisan passport:keys
```

## Compile frontend assets (on local setup)

**Run app**
```
$ npm run watch
```

**Compile assets and copy to public dir, dev env example**
```
$ npm run development
```
