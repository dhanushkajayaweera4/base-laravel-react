import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { withRouter, Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux'
import { performLogin } from '../../actions/auth';
import { RootState } from '../../reducers';

const Login = () => {
    const authError = useSelector((store: RootState) => {
        return store.auth.error;
    })
    const dispatch = useDispatch()
    const { register, handleSubmit, formState: { errors }, setError } = useForm();
    const [errorMessage, setErrorMessage] = useState(null);

    React.useEffect(() => {
        if (authError) {
            setErrorMessage(authError.message)
            if (authError.errors) {
                for (let key in authError.errors) {
                    if (authError.errors.hasOwnProperty(key)) {
                        setError(key, { type: 'validate', message: authError.errors[key][0] })
                    }
                }
            }
        }
    }, [authError])

    const login = function (data) {
        dispatch(performLogin(data.email, data.password));
    }
    let body = document.getElementsByTagName('body')[0]
    body.classList.remove('hold-transition', 'sidebar-mini', 'layout-fixed')
    body.classList.add('login-page')
    return (
        <div className="login-box">
            <div className="login-logo">
                <img src="/logo192.png" alt="Logo" />
            </div>
            <div className="card">
                <div className="card-body login-card-body">
                    <p className="login-box-msg">Iniciar sesión</p>
                    {
                        errorMessage &&
                        <div className="alert alert-danger">
                            <strong>{errorMessage}</strong>
                        </div>
                    }
                    <form onSubmit={handleSubmit(login)}>
                        <div className="form-group">
                            <label>E-mail</label>
                            <div className="input-group mb-3">
                                <input type="text" autoFocus
                                    {...register("email", { required: true })}
                                    className={'form-control ' + (errors.email ? 'is-invalid' : '')} />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope"></span>
                                    </div>
                                </div>
                                {
                                    errors.email && errors.email.type === 'required' &&
                                    <span className="invalid-feedback" role="alert"><strong>Please fill this field.</strong></span>
                                }
                                {
                                    errors.email && errors.email.type === 'validate' &&
                                    <span className="invalid-feedback" role="alert"><strong>{errors.email.message}</strong></span>
                                }
                            </div>
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <div className="input-group mb-3">
                                <input type="password"
                                    {...register("password", { required: true })}
                                    className={'form-control ' + (errors.password ? 'is-invalid' : '')} />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock"></span>
                                    </div>
                                </div>
                                {
                                    errors.password && errors.password.type === 'required' &&
                                    <span className="invalid-feedback" role="alert"><strong>Please fill this field.</strong></span>
                                }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-8">
                                <div className="icheck-primary">
                                    <input type="checkbox" id="remember" {...register("remember_me")} />
                                    <label htmlFor="remember">
                                        Recuérdame
                                    </label>
                                </div>
                            </div>
                            <div className="col-4">
                                <button type="submit" className="btn btn-primary btn-block">Login</button>
                            </div>
                        </div>
                        <p className="mb-1">
                            <Link to="/password/reset">Olvidé mi contraseña</Link>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default withRouter(Login)