<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Carbon\Carbon;

trait BasicExcelImport
{
    public function collection(Collection $rows)
    {
        if (!$this->isValid) {
            return;
        }
        $validator = Validator::make($rows->toArray(), $this->rules);
        if (method_exists($this, 'withValidator')) {
            $this->withValidator($validator);
        }
        if ($validator->fails()) {
            $this->isValid = false;
            $errors = $validator->errors()->toArray();
            $details = "";
            foreach ($errors as $key => $error) {
                $temp = explode('.', $key);
                $row = ((int) $temp[0]) + 2 + ($this->iter * $this->chunkSize());
                $field = $temp[1];
                $description = "Error en la fila $row, " . str_replace($key, ucwords(str_replace('_', ' ', $field)), $error[0]);
                $details .= $description.PHP_EOL;
            }
            $this->import->update([
                'status' => 'invalid_excel',
                'details' => $details,
                'finished_at' => Carbon::now(),
            ]);
            DB::table('data_imports')->where('import_id', $this->import->id)->delete();
            return;
        }
        foreach ($rows as $row) {
            // Insertar en la bd
            DB::table('data_imports')->insert(['data' => json_encode($row), 'import_id' => $this->import->id]);
        }
        DB::table('imports')->where('id', $this->import->id)->increment('percent_completed', 1, ['updated_at' => Carbon::now()]);
        $this->iter++;
    }

    public function chunkSize(): int
    {
        return 200;
    }
    
    /**
     * Is the import valid?
     * @return bool
     */
    public function isValid()
    {
        return $this->isValid;
    }
}
