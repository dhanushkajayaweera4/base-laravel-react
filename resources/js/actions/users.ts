import { IUsersState } from './../reducers/users';
import { IUser, TUserForm } from './../models/user';
import { ITableOptions } from './../models/tableoptions';
import { IApiError } from './../models/apierror';
import api from '../api';
import { logout, updateProfileSuccess, AuthActions } from './auth';
import { Dispatch } from 'react';
import { AppThunk } from '../store';
import { IApiNotification } from '../models/apinotification';

export const LOAD_USERS = 'LOAD_USERS'
export const LOAD_USERS_SUCCESS = 'LOAD_USERS_SUCCESS'
export const LOAD_USERS_ERROR = 'LOAD_USERS_ERROR'
export const CREATE_USER = 'CREATE_USER'
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS'
export const CREATE_USER_ERROR = 'CREATE_USER_ERROR'
export const UPDATE_USER = 'UPDATE_USER'
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS'
export const UPDATE_USER_ERROR = 'UPDATE_USER_ERROR'
export const DELETE_USER = 'DELETE_USER'
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS'
export const DELETE_USER_ERROR = 'DELETE_USER_ERROR'
export const TOGGLE_USER = 'TOGGLE_USER'
export const TOGGLE_USER_SUCCESS = 'TOGGLE_USER_SUCCESS'
export const TOGGLE_USER_ERROR = 'TOGGLE_USER_ERROR'
export const CLEAR_USERS_ERROR = 'CLEAR_ERROR'

interface ClearUsersError {
    type: typeof CLEAR_USERS_ERROR;
}
export function clearUsersError(): ClearUsersError {
    return {
        type: CLEAR_USERS_ERROR,
    }
}

function handleErrors(err: IApiError<TUserForm>, callback): any {
    return (dispatch) => {
        if (err.status === 401) {
            return dispatch(logout())
        }
        return callback()
    }
}

interface LoadUsers {
    type: typeof LOAD_USERS;
    options: ITableOptions;
}
export function loadUsers(options: ITableOptions): LoadUsers {
    return {
        type: LOAD_USERS,
        options
    }
}

interface LoadUsersSuccess {
    type: typeof LOAD_USERS_SUCCESS;
    data: IUser[];
    total: number;
}
export function loadUsersSuccess(data: IUser[], total: number): LoadUsersSuccess {
    return {
        type: LOAD_USERS_SUCCESS,
        data,
        total
    }
}

interface LoadUsersError {
    type: typeof LOAD_USERS_ERROR;
    error: IApiError<TUserForm>;
}
export function loadUsersError(error: IApiError<TUserForm>): LoadUsersError {
    return {
        type: LOAD_USERS_ERROR,
        error
    }
}

export function loadUsersIfNeeded(options: ITableOptions = null): AppThunk {
    return (dispatch, getState) => {
        if (getState().users.error !== null) {
            dispatch(clearUsersError())
        }
        if (shouldFetchUsers(getState().users)) {
            return dispatch(fetchUsers(options))
        }
    }
}

function shouldFetchUsers(state: IUsersState) {
    if (state.isLoading) {
        return false
    }
    if (state.data.length === 0) {
        return true
    }
    return false
}

export function fetchUsers(options: ITableOptions = null) {
    return dispatch => {
        dispatch(loadUsers(options))
        let filters = options == null ? null : {
            ...options.filters,
            page: options.pagination.page,
            take: options.pagination.take,
            sort: options.sort,
        }
        return api('get', '/users', filters)
            .then(response => dispatch(loadUsersSuccess(response.data.data, response.data.total)))
            .catch(err => dispatch(handleErrors(err, () => dispatch(loadUsersError(err)))))
    }
}

interface CreateUser {
    type: typeof CREATE_USER;
    userData: IUser;
}
export function createUser(userData: IUser): CreateUser {
    return {
        type: CREATE_USER,
        userData
    }
}

export function performCreateUser(userData: IUser) {
    return (dispatch) => {
        dispatch(createUser(userData));
        return api('post', '/users', userData)
            .then(response => {
                console.log(response.data);
                return response;
            })
            .then(response => {
                dispatch(fetchUsers())
                dispatch(createUserSuccess(userData, response.data.message))
            })
            .catch(err => dispatch(handleErrors(err, () => dispatch(createUserError(err)))))
    }
}

interface CreateUserSuccess {
    type: typeof CREATE_USER_SUCCESS;
    createdUser: IUser;
    notification: IApiNotification;
}
export function createUserSuccess(createdUser: IUser, message: string): CreateUserSuccess {
    return {
        type: CREATE_USER_SUCCESS,
        notification: {
            type: "success",
            message
        },
        createdUser
    }
}

interface CreateUserError {
    type: typeof CREATE_USER_ERROR;
    notification: IApiNotification;
}
export function createUserError(error: IApiError<TUserForm>): CreateUserError {
    return {
        type: CREATE_USER_ERROR,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

interface UpdateUser {
    type: typeof UPDATE_USER;
    userData: IUser;
}
export function updateUser(userData: IUser): UpdateUser {
    return {
        type: UPDATE_USER,
        userData
    }
}

export function performUpdateUser(userData: IUser) {
    return (dispatch: Dispatch<UsersActions | AuthActions>, getState) => {
        dispatch(updateUser(userData));
        return api('put', `/users/${userData.id}`, userData)
            .then(response => {
                // Si se actualizó el usuario logueado
                if (getState().auth.auth.user.id == userData.id) {
                    dispatch(updateProfileSuccess(response.data.data, "Perfil actualizado satisfactoriamente."))
                }
                dispatch(updateUserSuccess(response.data.data, response.data.message))
            })
            .catch(err => dispatch(handleErrors(err, () => dispatch(updateUserError(err)))))
    }
}

interface UpdateUserSuccess {
    type: typeof UPDATE_USER_SUCCESS;
    updatedUser: IUser;
    notification: IApiNotification;
}
export function updateUserSuccess(updatedUser: IUser, message: string): UpdateUserSuccess {
    return {
        type: UPDATE_USER_SUCCESS,
        updatedUser,
        notification: {
            type: "success",
            message
        },
    }
}

interface UpdateUserError {
    type: typeof UPDATE_USER_ERROR;
    notification: IApiNotification;
}
export function updateUserError(error: IApiError<TUserForm>): UpdateUserError {
    return {
        type: UPDATE_USER_ERROR,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

interface DeleteUser {
    type: typeof DELETE_USER;
    userData: IUser;
}
export function deleteUser(userData: IUser): DeleteUser {
    return {
        type: DELETE_USER,
        userData
    }
}

export function performDeleteUser(userData: IUser) {
    return dispatch => {
        dispatch(deleteUser(userData));
        return api('delete', `/users/${userData.id}`)
            .then(response => dispatch(deleteUserSuccess(response.data.message)))
            .catch(err => dispatch(handleErrors(err, () => dispatch(deleteUserError(err)))))
    }
}

interface DeleteUserSuccess {
    type: typeof DELETE_USER_SUCCESS;
    notification: IApiNotification;
}
export function deleteUserSuccess(message: string) {
    return {
        type: DELETE_USER_SUCCESS,
        notification: {
            type: "success",
            message
        },
    }
}

interface DeleteUserError {
    type: typeof DELETE_USER_ERROR;
    notification: IApiNotification;
}
export function deleteUserError(error: IApiError<TUserForm>): DeleteUserError {
    return {
        type: DELETE_USER_ERROR,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

interface ToggleUser {
    type: typeof TOGGLE_USER;
    userData: IUser;
}
export function toggleUser(userData: IUser): ToggleUser {
    return {
        type: TOGGLE_USER,
        userData
    }
}

export function performToggleUser(userData: IUser) {
    return (dispatch: Dispatch<UsersActions>) => {
        dispatch(toggleUser(userData));
        return api('put', `/users/${userData.id}/status`)
            .then(response => dispatch(toggleUserSuccess(userData.is_active ? false : true, response.data.message)))
            .catch(err => dispatch(handleErrors(err, () => dispatch(toggleUserError(err)))))
    }
}

interface ToggleUserSuccess {
    type: typeof TOGGLE_USER_SUCCESS;
    isActive: boolean;
    notification: IApiNotification;
}
export function toggleUserSuccess(isActive: boolean, message: string): ToggleUserSuccess {
    return {
        type: TOGGLE_USER_SUCCESS,
        isActive,
        notification: {
            type: "success",
            message
        },
    }
}

interface ToggleUserError {
    type: typeof TOGGLE_USER_ERROR;
    notification: IApiNotification;
}
export function toggleUserError(error: IApiError<TUserForm>): ToggleUserError {
    return {
        type: TOGGLE_USER_ERROR,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

export type UsersActions = LoadUsers
    | LoadUsersSuccess
    | LoadUsersError
    | CreateUser
    | CreateUserSuccess
    | CreateUserError
    | UpdateUser
    | UpdateUserSuccess
    | UpdateUserError
    | DeleteUser
    | DeleteUserSuccess
    | DeleteUserError
    | ToggleUser
    | ToggleUserSuccess
    | ToggleUserError
    | ClearUsersError