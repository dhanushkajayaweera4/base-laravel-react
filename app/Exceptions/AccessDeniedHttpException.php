<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AccessDeniedHttpException extends HttpException
{
    /**
     * Error code
     * @var string 
     */
    protected $code;

    /**
     * Create a new resource exception instance.
     *
     * @param string                               $message
     * @param \Exception                           $previous
     * @param array                                $headers
     * @param int                                  $code
     *
     * @return void
     */
    public function __construct($message = null, $errorCode = 'not_allowed', Exception $previous = null, $headers = [], $code = 0)
    {
        $this->code = $errorCode;
        parent::__construct(403, $message, $previous, $headers, $code);
    }
}
