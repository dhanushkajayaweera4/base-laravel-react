import { IRole } from './../models/role';
import {
    LOAD_CATALOG_ROLES,
    LOAD_CATALOG_ROLES_SUCCESS,
    CatalogsActions
} from '../actions';

export interface ICatalogsState {
    roles: IRole[];
    lastAction: string;
}

export const initialState: ICatalogsState = {
    roles: [],
    lastAction: null
};

export function reducer(
    state = initialState,
    action: CatalogsActions
): ICatalogsState {
    switch (action.type) {
        case LOAD_CATALOG_ROLES:
            return { ...state, lastAction: action.type };
        case LOAD_CATALOG_ROLES_SUCCESS:
            return { ...state, roles: action.data, lastAction: action.type };
        default:
            return state;
    }
}