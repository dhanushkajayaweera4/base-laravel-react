export interface ITableOptions {
    include?: string;
    pagination: {
        page: number,
        take: number
    };
    sort: string;
    filters: Object
}
