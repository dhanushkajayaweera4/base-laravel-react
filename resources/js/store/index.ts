import { CatalogsActions } from './../actions/catalogs';
import { UsersActions } from './../actions/users';
import { AuthActions } from './../actions/auth';
import { createStore, applyMiddleware, compose, Store, AnyAction } from 'redux'
import thunk, { ThunkAction } from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer, { RootState } from '../reducers'
import { loadState, saveState } from './localstorage'
import { NotificationsActions } from '../actions';

const initialData = loadState();

const store: Store<RootState, AuthActions | UsersActions | CatalogsActions | NotificationsActions> = createStore(
    rootReducer,
    initialData,
    process.env.NODE_ENV === 'production'
        ? compose(applyMiddleware(thunk, createLogger()))
        : composeWithDevTools(applyMiddleware(thunk, createLogger()))
)
store.subscribe(function () {
    saveState(store.getState())
})

export type AppDispatch = typeof store.dispatch

if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('../reducers', () => {
        store.replaceReducer(rootReducer)
    })
}
export default store

export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, AnyAction>