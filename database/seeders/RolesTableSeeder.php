<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin' => "Administrador",
        ];
        foreach ($roles as $slug => $name) {
            $role = Role::where('slug', $slug)->first();
            if ($role == null) {
                $role = Role::create(['slug' => $slug, 'name' => $name]);
            }
            else {
                $role->update(['name' => $name]);
            }
        }
    }
}
