import { Link } from 'react-router-dom';

const NoMatch = () => {
    return (
        <div className="h-100 bg-plum-plate bg-animation">
            <div className="d-flex h-100 justify-content-center align-items-center">
                <div className="mx-auto app-login-box col-md-8">
                    <div className="app-logo-inverse mx-auto mb-3"></div>
                    <div className="error-page">
                        <h2 className="headline text-warning"> 404</h2>
                        <div className="error-content">
                            <h3><i className="fa fa-warning text-warning"></i> ¡Oops! Página no encontrada.</h3>
                            <p>
                                La página a la que intentas acceder no existe, intenta
                            </p>
                            <div className="row">
                                <div className="col-auto">
                                    <Link to="/home" className="btn btn-primary">Ir a la página de inicio</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NoMatch;