<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('oauth_clients')->count() === 0) {
            $this->createOauthClients();
        }
        // Crear roles
        $this->call(RolesTableSeeder::class);
        // Crear usuario default
        $this->call(AdminUserSeeder::class);
    }

    /**
     * Generate oauth clients
     */
    private function createOauthClients()
    {
        $clientPassword = [
            'id' => 1,
            'name' => 'SySkul Password Grant Client',
            'secret' => App::environment('local') ? 'Uo0J0g8WA4YOMWh8HqRHZDpSGzrFnGLRuYwMJMbj'
                    : (App::environment('production') ? 'hSF1PTwCCXlKDPjwUEcybPGu8h0tT4iZ2h9kMN3I'
                    : 'tOdTGvJzt9PMVXsNkQGumFDKg5tIQTHr7KL5tBDU'),
            'provider' => 'users',
            'redirect' => 'http://localhost',
            'personal_access_client' => false,
            'password_client' => true,
            'revoked' => false
        ];
        DB::table('oauth_clients')->insert([
            'id' => $clientPassword['id'],
            'name' => $clientPassword['name'],
            'secret' => $clientPassword['secret'],
            'provider' => $clientPassword['provider'],
            'redirect' => $clientPassword['redirect'],
            'personal_access_client' => $clientPassword['personal_access_client'],
            'password_client' => $clientPassword['password_client'],
            'revoked' => $clientPassword['revoked'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::statement('ALTER SEQUENCE oauth_clients_id_seq RESTART WITH 1');
    }
}
