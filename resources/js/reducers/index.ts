import { CatalogsActions } from './../actions/catalogs';
import { UsersActions } from './../actions/users';
import { AuthActions } from './../actions/auth';
import { IAuthState, reducer as authReducer } from './auth';
import { IUsersState, reducer as userReducer } from './users';
import { ICatalogsState, reducer as catalogReducer } from './catalogs';
import { combineReducers } from 'redux';
import { IErrorState, reducer as notificationsReducer } from './notifications';
import { NotificationsActions } from '../actions';

export type RootState = {
    auth: IAuthState;
    users: IUsersState;
    catalogs: ICatalogsState,
    notifications: IErrorState,
}
const rootReducer = combineReducers<RootState, AuthActions | UsersActions | CatalogsActions | NotificationsActions>({
    auth: authReducer,
    users: userReducer,
    catalogs: catalogReducer,
    notifications: notificationsReducer
})

export default rootReducer